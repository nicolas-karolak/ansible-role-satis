---

- name: "install packages"
  apt:
    force_apt_get: true
    install_recommends: false
    cache_valid_time: 86400
    name: "{{ nginx_packages }}"

- name: "override main configuration if defined"
  when: "nginx_main | d(none, true)"
  notify: "restart nginx"
  template:
    src: "nginx.conf"
    dest: "/etc/nginx/nginx.conf"
    validate: "nginx -t -c %s"

- name: "configure snippets"
  loop: "{{ nginx_snippets }}"
  loop_control:
    label: "{{ item.name }}"
  notify: "restart nginx"
  template:
    src: "blocks.conf"
    dest: "/etc/nginx/snippets/{{ item.name }}.conf"

- name: "configure htpasswd"
  loop: "{{ nginx_htpasswd }}"
  loop_control:
    label: "{{ item.name }}"
  htpasswd:
    path: "{{ item.path }}"
    owner: "root"
    group: "www-data"
    mode: 0640
    name: "{{ item.name }}"
    password: "{{ item.password }}"

- name: "configure conf.d"
  loop: "{{ nginx_confd }}"
  loop_control:
    label: "{{ item.name }}"
  notify: "restart nginx"
  template:
    src: "blocks.conf"
    dest: "/etc/nginx/conf.d/{{ item.name }}.conf"
    validate: "bash -c 'nginx -t -c /dev/stdin <<< \"{{ _nginx_validate }} include %s;\"'"

- name: "configure sites-available"
  loop: "{{ nginx_sites }}"
  loop_control:
    label: "{{ item.name }}"
  notify: "restart nginx"
  template:
    src: "blocks.conf"
    dest: "/etc/nginx/sites-available/{{ item.name }}"
    validate: "bash -c 'nginx -t -c /dev/stdin <<< \"{{ _nginx_validate }} http { include %s; }\"'"

- name: "enable sites"
  loop: "{{ nginx_sites }}"
  loop_control:
    label: "{{ item.name }}"
  when: "item.enabled | d(false)"
  notify: "restart nginx"
  file:
    src: "/etc/nginx/sites-available/{{ item.name }}"
    dest: "/etc/nginx/sites-enabled/{{ item.name }}"
    state: "link"

- name: "remove configurations files"
  notify: "restart nginx"
  loop: "{{ nginx_removes }}"
  file:
    path: "/etc/nginx/{{ item }}"
    state: "absent"

- meta: "flush_handlers"
